// Blatt 1, Aufgabe 1.3a: Basistypen
// Gruppe 1: Nils Bröcker (802413), Adrian Henze (804155)
#include <stdio.h>
#include <limits.h>


int main() {
  puts("");
  
  // Tabellenkopf.
  printf("data type\tsize\t\tmin\t\t\tmax\t\t\tumin\t\tumax\n");

  // Char.
  printf("\nchar\t\t%d\t\t%d\t\t\t%d\t\t\t%u\t\t%u\n", (int)sizeof(char), (int)CHAR_MIN, (int)CHAR_MAX, (unsigned char)0, (unsigned char)UCHAR_MAX);

  // Short.
  printf("\nshort\t\t%d\t\t%d\t\t\t%d\t\t\t%u\t\t%u\n", (int)sizeof(short), (short)SHRT_MIN, (short)SHRT_MAX, (unsigned short)0, (unsigned short)USHRT_MAX);

  // Int.
  printf("\nint\t\t%d\t\t%d\t\t%d\t\t%u\t\t%u\n", (int)sizeof(int), (int)INT_MIN, (int)INT_MAX, (unsigned int)0, (unsigned int)UINT_MAX);

  // Long.
  printf("\nlong\t\t%d\t\t%ld\t%ld\t%lu\t\t%lu\n", (int)sizeof(long), (long)LONG_MIN, (long)LONG_MAX, (unsigned long)0, (unsigned long)ULONG_MAX);

  // Long Long.
  printf("\nlong long\t%d\t\t%lld\t%lld\t%llu\t\t%llu\n", (int)sizeof(long long), (long long)LLONG_MIN, (long long)__LONG_LONG_MAX__, (unsigned long long)0, (unsigned long long)ULONG_MAX);

  // Float.
  printf("\nfloat\t\t%d\t\t%e\t\t%e\t\t-\t\t-\n", (int)sizeof(float), (float)__FLT_MIN__, (float)__FLT_MAX__);

  // Double.
  printf("\ndouble\t\t%d\t\t%le\t\t%le\t\t-\t\t-\n", (int)sizeof(double), (double)__DBL_MIN__, (double)__DBL_MAX__);

  // Long Double.
  printf("\nlong double\t%d\t\t%Le\t\t%Le\t\t-\t\t-\n", (int)sizeof(long double), (long double)__LDBL_MIN__, (long double)__LDBL_MAX__);

  // Void.
  printf("\nvoid\t\t%d\t\t-\t\t\t-\t\t\t-\t\t-\n", (int)sizeof(void));

  puts("");
  return 0;
}
