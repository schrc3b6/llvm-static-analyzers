// Blatt 1, Aufgabe 1.5a: Ringspeicher
// Gruppe 1: Nils Bröcker (802413), Adrian Henze (804155)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define SIZE 100		// Konstante für besser variable Ringspeichergröße


typedef struct { // Struct für Daten.
	char character;
} userdata_t;


typedef struct { // Struct zur Listenverwaltungs
	int readIndex;
	int writeIndex;
	userdata_t *fifo; // Eigentlicher Ringbuffer
	int size; // Buffergröße
} ringbuffer_helper_t;


// Funktion zum Anlegen | Size: Buffergröße | R-Wert: Pointer auf den Helfer
ringbuffer_helper_t *createFIFO(int size) {
	// Pointer auf Helper + Speicherreservierung
	ringbuffer_helper_t *rbuffer = (ringbuffer_helper_t *)malloc(sizeof(ringbuffer_helper_t));
	// Wertezuweisung
	rbuffer->readIndex = 0;
	rbuffer->writeIndex = 0;
	rbuffer->fifo = (userdata_t *)malloc(sizeof(userdata_t) * (size + 1));
	rbuffer->size = size;
	// Pointer-Return
	return rbuffer;
}


// Funktion, zum Anhängen weiterer Elemente | Data: zu schreibende Daten | kein R-Wert
void writeFIFO(userdata_t data, ringbuffer_helper_t *rbuffer) {
	if (rbuffer) {
		rbuffer->fifo[rbuffer->writeIndex] = data; // Daten in den entprechenden Index schreiben
		rbuffer->writeIndex = rbuffer->writeIndex++ % (rbuffer->size+1); // Hier overflow möglich, leider kein Weg zum fixen gefunden :-(
		if(rbuffer->readIndex == rbuffer->writeIndex)
			rbuffer->readIndex = rbuffer->readIndex ++ % (rbuffer->size+1);
	}
}


// Funktion, zum Lesen
int readFIFO(userdata_t *data, ringbuffer_helper_t *rbuffer) {
	if (rbuffer) {
		if(rbuffer->readIndex != rbuffer->writeIndex) {
			*data = rbuffer->fifo[rbuffer->readIndex];
			rbuffer->readIndex = rbuffer->readIndex++ % (rbuffer->size+1);
			return 0;  // alles OK
		}
		else
			return -1; // keine gültigen Daten
	}
	else
		return -1; // kein gültiger Helper
}
