// Blatt 1, Aufgabe 1.5a: Ringspeicher
// Gruppe 1: Nils Bröcker (802413), Adrian Henze (804155)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define SIZE 100		// Konstante für besser variable Ringspeichergröße


typedef struct { // Struct für Daten.
	char character;
} userdata_t;


typedef struct { // Struct zur Listenverwaltungs
	int readIndex;
	int writeIndex;
	userdata_t *fifo; // Eigentlicher Ringbuffer
	int size; // Buffergröße
} ringbuffer_helper_t;


// Funktion zum Anlegen | Size: Buffergröße | R-Wert: Pointer auf den Helfer
ringbuffer_helper_t *createFIFO(int size) ;

// Funktion, zum Anhängen weiterer Elemente | Data: zu schreibende Daten | kein R-Wert
void writeFIFO(userdata_t data, ringbuffer_helper_t *rbuffer) ;


// Funktion, zum Lesen
int readFIFO(userdata_t *data, ringbuffer_helper_t *rbuffer) ;
