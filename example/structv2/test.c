#include <string.h>
#include <stdlib.h>

typedef struct { // Struct für Daten.
	char character;
} userdata_t;


typedef struct { // Struct zur Listenverwaltungs
	int readIndex;
	int writeIndex;
	userdata_t *fifo; // Eigentlicher Ringbuffer
	int size; // Buffergröße
} ringbuffer_helper_t;

ringbuffer_helper_t *createFIFO(int size) {
	// Pointer auf Helper + Speicherreservierung
	ringbuffer_helper_t *rbuffer = (ringbuffer_helper_t *)malloc(sizeof(ringbuffer_helper_t));
	// Wertezuweisung
	rbuffer->readIndex = 0;
	rbuffer->writeIndex = 0;
	rbuffer->fifo = (userdata_t *)malloc(sizeof(userdata_t) * (size + 1));
	rbuffer->size = size;
	// Pointer-Return
	return rbuffer;
}
