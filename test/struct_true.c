#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct test{
    int value;
    int* mem;
};


int main(int argc, char** argv, char** envp)
{
    struct test t;
    t.value=8;
    t.mem = malloc(sizeof(int));
    //scanf("%d",&t.value);
    if(t.value<8) {
        printf("%d",t.value);
    } else {
        *t.mem=4; //expected-warning {{Using Variable before checking it for Errors}}
        printf("%d, %d",*t.mem,t.value);
    }
    memcpy(t.mem,&t.value,sizeof(int)); //expected-warning {{Using Variable before checking it for Errors}}
    free(t.mem);
}
